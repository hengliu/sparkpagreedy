#Author: Heng Liu
#Contact: hengl@email.arizona.edu


import numpy as np
import ctypes as c

Tool = c.CDLL('libMIToolbox.so')


# Use to calculate the conditional mutual information of two random variables given a vector: I(x,y|z)
# The function in MIToolbox is 'calculateConditionalMutualInformation(double *dataVector, double *targetVector, double *conditionVector, int vectorLength)'
# Take in three random variables, all 1d vectors
# Return the conditional mutual information (double)
def CMI(x, y, z):

  n_observations = x.shape[0]

  x = 1.0*np.array(x, order="F")
  y = 1.0*np.array(y, order="F")
  z = 1.0*np.array(z, order="F")

  function = Tool.calculateConditionalMutualInformation
  function.restype = c.c_double

  result = function( x.ctypes.data_as(c.POINTER(c.c_double)), y.ctypes.data_as(c.POINTER(c.c_double)), z.ctypes.data_as(c.POINTER(c.c_double)), c.c_int(n_observations))
  return result



# Use to calculate the mutual information of two random variables I(x,y)
# The function in MIToolbox is 'calculateMutualInformation(double *dataVector, double *targetVector, int vectorLength)' 
# Take in two random variables
# Return the mutual information (double)
def MI(x, y):

  n_observations = x.shape[0]

  x = 1.0*np.array(x, order="F")
  y = 1.0*np.array(y, order="F")

  function = Tool.calculateMutualInformation
  function.restype = c.c_double

  result = function( x.ctypes.data_as(c.POINTER(c.c_double)), y.ctypes.data_as(c.POINTER(c.c_double)), c.c_int(n_observations))
  return result



# Use to construct the joint random variable of two random variables (x,y)
# The function in MIToolbox is 'mergeArrays(double *firstVector, double *secondVector, double *outputVector, int vectorLength)'
# Take in three random variables, all numpy 1d vector
# The third random variable is used to return the result
def Joint(x, y, z):

  n_observations = x.shape[0]

  x = 1.0*np.array(x, order="F")
  y = 1.0*np.array(y, order="F")
  z = 1.0*np.array(z, order="F")

  function = Tool.mergeArrays
  function.restype = c.c_int

  result = function( x.ctypes.data_as(c.POINTER(c.c_double)), y.ctypes.data_as(c.POINTER(c.c_double)), z.ctypes.data_as(c.POINTER(c.c_double)), c.c_int(n_observations))
  return z



# Use to calculate the entropy of random variable data: H(data)
# The function in MIToolbox is 'calculateEntropy(double *dataVector, int vectorLength)'
# Take in the random variable numpy 1d vector
# Return the entropy (double)
def Entropy(data):

  n_observations = data.shape[0]
  data = 1.0*np.array(data, order="F")

  function = Tool.calculateEntropy
  function.restype = c.c_double

  result = function(data.ctypes.data_as(c.POINTER(c.c_double)), c.c_int(n_observations))
  return result



# Use to calculate the Joint entropy of random variable x and y: H(x,y)
# The function in MIToolbox is 'calculateJointEntropy(double *firstVector, double *secondVector, int vectorLength)'
# Take in 2 random variables both numpy 1d vector
# Return the joint entropy (double)
def JointEntropy(x, y):

  n_observations = x.shape[0]

  x = 1.0*np.array(x, order="F")
  y = 1.0*np.array(y, order="F")

  function = Tool.calculateJointEntropy
  function.restype = c.c_double

  result = function( x.ctypes.data_as(c.POINTER(c.c_double)), y.ctypes.data_as(c.POINTER(c.c_double)), c.c_int(n_observations))
  return result



# Use to calculate the Conditional entropy of random variable x given y: H(x|y)
# The function in MIToolbox is 'calculateConditionalEntropy(double *dataVector, double *conditionVector, int vectorLength)'
# Take in 2 random variables both numpy 1d vector
# Return the conditional entropy (double)
def CondEntropy(x, y):

  n_observations = x.shape[0]

  x = 1.0*np.array(x, order="F")
  y = 1.0*np.array(y, order="F")

  function = Tool.calculateConditionalEntropy
  function.restype = c.c_double

  result = function( x.ctypes.data_as(c.POINTER(c.c_double)), y.ctypes.data_as(c.POINTER(c.c_double)), c.c_int(n_observations))
  return result



# check the data
def check_data(data, labels):
  if isinstance(data, np.ndarray) is False:
    raise Exception("data must be an numpy ndarray.")
  if isinstance(labels, np.ndarray) is False:
    raise Exception("labels must be an numpy ndarray.")

  if len(data) != len(labels):
    raise Exception("data and labels must be the same length")

  return 1.0*np.array(data, order="F"), 1.0*np.array(labels, order="F")

