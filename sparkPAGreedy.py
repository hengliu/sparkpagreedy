# Author: Heng Liu
# Contact: hengl@email.arizona.edu

from pyspark import SparkContext as sc
import scipy.io as sio
import numpy as np
import time
import IT

class sparkPAGreedy(object):
	
	def __init__(self, path, name, k, M):
		# path: the input data path
		# name: data name (string)
		# k: the number of features to select
		# M: the parallelism level	
		self.name = name
		self.k = k
		self.M = M
		data = sio.loadmat(path)[name]
		self.n_samples, n_features = data.shape
		self.n_features = n_features - 1
		self.boot_indices = np.random.randint(self.n_samples, size = [self.n_samples])
		data = data[self.boot_indices ,:]

		self.data = data[:, :self.n_features]
		self.label = data[:, self.n_features]

		self.features = range(self.n_features)
		self.start_time = time.time()

		self.selected = []
		self.times = [0]

	def selection(self):
		selected = self.selected
		times = self.times
		indices = sc.parallelize(self.features, numSlices = self.M)
		# We first select the first feature by mapping the feature indices to key-valu pair (MI w/ label, feature index)
		maximum = indices.map(lambda x: (IT.MI(data[:, x], self.label), x) ).max() 
		selected.append(maximum[1])
		times.append(time.time() - self.start_time)
		# remove the first selected feature, and map the features to key value pair, (ScoreUpdated, feature_index), and sort the pairs to descending order
		indices = indices.filter(lambda x: x[1] != maximum[1]).map(lambda x: (0.0, x)).map(lambda x: self.ScoreUpdate(x, selected)).sortByKey(False)
		
		while len(selected) < self.k:
			# We find top M+1 candidates and identify 2nd->M+1 features using the rdd.top method, and pass the rule "key = lambda x: x[1]" which is order by x[1]. 
			ToIdentify = sc.parallelize(indices.top(self.M + 1, key = lambda x: x[1]))
			# Then we map the feature2identify to different interfering zone, finally we pick out the largest interfering zone: IZ
			maximum = ToIdentify.top(1)
			sequence = indices.collect()
			IZ = ToIdentify.map(lambda x: self.FindIZ(x, maximum, sequence)).top(1)
			Ident_MaxIntrd = ToIdentify.map(lambda x: self.Identification(x, IZ, maximum, sequence)).collect()
			# Based on ident&maxIntrd results, we select multiple features
			current_selected = []
			for i in range(len(Ident_MaxIntrd)):
				x = Ident_MaxIntrd[i]
				if x[0][0] == 1:  #identified as a Hit
					current_selected.append(x[1])
				else:
					if i != len(Ident_MaxIntrd) - 1:
						current_selected.append(Ident_MaxIntrd[i + 1][1])
					break
			indices = indices.filter(lambda x: x[1] not in current_selected).map(lambda x: self.ScoreUpdate(x, current_selected)).sortByKey(False)
			times.append(time.time() - self.start_time)
		self.selected = selected
		self.times = times

	def ScoreUpdate(self, x, selected):
		# here we update the score for a feature, we use temp vector to store the merged vector
		temp = x
		merged_vector = self.data[:, temp[1]] 
		for i in selected:
			merged_vector = IT.Joint(self.data[:, temp[1]], self.data[:, i], merged_vector)
			temp[0] = temp[0] + IT.MI(merged_vector, self.label)
		return temp

	def FindIZ(self, x, maximum, sequence):
		data = self.data
		if x != maximum:
			position = sequence.index(x)
			boundary = 0.0
			for i in range(position):
				merged_vector = data[:, x[1]]
				temp = IT.MI(IT.Joint(data[:, x[1]], data[:, sequence[i][1]], merged_vector), self.label)
				boundary = boundary + temp - IT.Entropy(self.label)
			windowsize = 1
			for i in range(position + 1, len(sequence)):
				if x[0] - sequence[i][0] + boundary < 0:
					windowsize += 1
				else:
					break
			return windowsize + position
		else:
			return -1

	def Identification(self, x, IZ, maximum, sequence):
		data = self.data
		flag = (1, -1)
		if x != maximum:
			position = sequence.index(x)
			base = 0.0
			for i in range(position):
				merged_vector = data[:, x[1]]
				base = base + IT.MI(IT.Joint(data[:, x[1]], data[:, sequence[i][1]], merged_vector), self.label)
			intruder_max_objective = 0.0
			for i in range(position + 1, IZ):
				objective = x[0] - sequence[i][0] + base
				for j in range(position):
					merged_vector = data[:, x[1]]
					objective = objective - IT.Entropy( IT.Joint( data[:, x[1]], data[:, sequence[i][1]], merged_vector ), self.label )
				if objective < 0:
					flag[0] = -1
					if x[0] + base - objective > intruder_max_objective:
						flag[1] = sequence[i][1]
						intruder_max_objective = x[0] + base - objective
			return (flag, x[1])
		else:
			return ((1, -1), x[1])
		



	
