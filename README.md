This project is the implementation of PAGreedy algorithm using Pyspark, for details of the algorithm, please refer to "A semi-parallel greedy optimization for information theoretic feature selection".


-----------------------------
step 1: we need to install this wrapper of Gavin Brown's MIToolbox

Installation:
python ./setup.py build
sudo python ./setup.py install

step 2: submit the pyspark script

